package net.ihe.gazelle.ui.services;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named("linkDataProviderService")
public class LinkDataProviderService {

    @Inject
    private Instance<LinkDataProvider> providers;

    private LinkDataProviderService() {
        super();
    }

    /**
     * @param valueClass class to be supported
     *
     * @return a provider supporting this valueClass
     */
    public LinkDataProvider getProviderForClass(Class<?> valueClass) {
        for (LinkDataProvider dataProvider : providers) {
            List<Class<?>> supportedClasses = dataProvider.getSupportedClasses();
            for (Class<?> supportedClass : supportedClasses) {
                if (supportedClass.isAssignableFrom(valueClass)) {
                    return dataProvider;
                }
            }
        }
        return null;
    }
}

package net.ihe.gazelle.ui.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ResourceBundle;

@Named("translateProvider")
public class TranslateProviderImpl implements TranslateProvider {

	private static Logger log = LoggerFactory.getLogger(TranslateProviderImpl.class);

	@Override
	public String getTranslation(String key) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String translation = null;
		ResourceBundle bundle = null;
		try{
			bundle = FacesContext.getCurrentInstance().getApplication()
					.getResourceBundle(FacesContext.getCurrentInstance(), "messages");
		}catch (Exception e){
			log.error("Error retrieving message bundle");
			log.error("", e);
		}
		if (bundle != null && key != null && bundle.containsKey(key)){
			translation = bundle.getString(key);
		}
		else {
			log.info("Error looking for translation with key : {} : key not found in bundle", key);
		}
		return translation == null ? key : translation;
	}
}

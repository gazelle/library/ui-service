package net.ihe.gazelle.ui.services;

import javax.enterprise.inject.spi.CDI;

public class TranslateService {

	private TranslateService() {
		super();
	}

	private static TranslateProvider getProvider() {
		return CDI.current().select(TranslateProvider.class).get();
	}

	public static String getTranslation(String key) {
		return getProvider().getTranslation(key);
	}
}

package net.ihe.gazelle.ui.services;

public interface TranslateProvider {

	String getTranslation(String key);

}
